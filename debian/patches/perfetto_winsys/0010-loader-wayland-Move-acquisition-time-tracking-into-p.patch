From: Derek Foreman <derek.foreman@collabora.com>
Date: Wed, 29 Jan 2025 11:18:52 -0600
Subject: [PATCH] loader/wayland: Move acquisition time tracking into perfetto
 flows

We only use the acquisition time for calculating latency for perfetto
tracks later, and the acquisition time should ideally be the start of the
perfetto flow.

This has been more or less true with very small error margin for vk wsi,
but the wayland EGL buffer handling is a lot more complicated. Moving the
time check into the flow start will make re-using this code for EGL much
simpler.

Signed-off-by: Derek Foreman <derek.foreman@collabora.com>
---
 src/loader/loader_wayland_helper.c  | 26 +++++++++++---------------
 src/loader/loader_wayland_helper.h  |  7 ++++---
 src/util/perf/cpu_trace.h           | 18 ++++++++++++++----
 src/vulkan/wsi/wsi_common_wayland.c | 17 ++++++++---------
 4 files changed, 37 insertions(+), 31 deletions(-)

diff --git a/src/loader/loader_wayland_helper.c b/src/loader/loader_wayland_helper.c
index 8ed21da..fc7ca7e 100644
--- a/src/loader/loader_wayland_helper.c
+++ b/src/loader/loader_wayland_helper.c
@@ -36,12 +36,11 @@
 struct loader_wayland_presentation_feedback_data {
    struct loader_wayland_presentation *presentation;
    bool tracing;
-   uint64_t flow_id;
+   struct mesa_trace_flow flow;
    /* We store copies of name and id, since buffers can be
     * destroyed before feedback is serviced */
    char *buffer_name;
    uint32_t buffer_id;
-   uint64_t buffer_acquisition_time;
    void *callback_data;
    struct wp_presentation_feedback *feedback;
    struct list_head link;
@@ -201,9 +200,8 @@ loader_wayland_wrap_buffer(struct loader_wayland_buffer *lwb,
 {
    lwb->buffer = wl_buffer;
    lwb->id = wl_proxy_get_id((struct wl_proxy *)wl_buffer);
-   lwb->flow_id = 0;
+   lwb->flow.id = 0;
    lwb->name = stringify_wayland_id(lwb->id);
-   lwb->acquisition_time = 0;
 }
 
 void
@@ -212,17 +210,16 @@ loader_wayland_buffer_destroy(struct loader_wayland_buffer *lwb)
    wl_buffer_destroy(lwb->buffer);
    lwb->buffer = NULL;
    lwb->id = 0;
-   lwb->flow_id = 0;
+   lwb->flow.id = 0;
    free(lwb->name);
    lwb->name = NULL;
-   lwb->acquisition_time = 0;
 }
 
 void
-loader_wayland_buffer_set_flow(struct loader_wayland_buffer *lwb, uint64_t flow_id)
+loader_wayland_buffer_set_flow(struct loader_wayland_buffer *lwb,
+                               struct mesa_trace_flow *flow)
 {
-  lwb->flow_id = flow_id;
-  lwb->acquisition_time = os_time_get_nano();
+  lwb->flow = *flow;
 }
 
 bool
@@ -274,7 +271,7 @@ loader_wayland_trace_present(struct loader_wayland_presentation_feedback_data *f
    clock = fd->presentation->clock_id;
 
    MESA_TRACE_SET_COUNTER(lws->analytics.latency_str,
-                          (presentation_time - fd->buffer_acquisition_time) / 1000000.0);
+                          (presentation_time - fd->flow.start_time) / 1000000.0);
 
    /* Close the previous image display interval first, if there is one. */
    if (lws->analytics.presenting) {
@@ -287,7 +284,7 @@ loader_wayland_trace_present(struct loader_wayland_presentation_feedback_data *f
 
    MESA_TRACE_TIMESTAMP_BEGIN(fd->buffer_name,
                               lws->analytics.presentation_track_id,
-                              fd->flow_id,
+                              fd->flow.id,
                               clock, presentation_time);
 }
 
@@ -322,7 +319,7 @@ presentation_handle_presented(void *data,
    struct timespec presentation_ts;
    uint64_t presentation_time;
 
-   MESA_TRACE_FUNC_FLOW(&fd->flow_id);
+   MESA_TRACE_FUNC_FLOW(&fd->flow);
 
    presentation_ts.tv_sec = ((uint64_t)tv_sec_hi << 32) + tv_sec_lo;
    presentation_ts.tv_nsec = tv_nsec;
@@ -343,7 +340,7 @@ presentation_handle_discarded(void *data,
    struct loader_wayland_presentation_feedback_data *fd = data;
    struct loader_wayland_presentation *pres = fd->presentation;
 
-   MESA_TRACE_FUNC_FLOW(&fd->flow_id);
+   MESA_TRACE_FUNC_FLOW(&fd->flow);
 
    if (pres->discarded_callback)
       pres->discarded_callback(fd->callback_data);
@@ -417,11 +414,10 @@ loader_wayland_presentation_feedback(struct loader_wayland_presentation *pres,
    fd = malloc(sizeof *fd);
    fd->presentation = pres;
    fd->tracing = tracing;
-   fd->flow_id = lwb->flow_id;
    if (tracing) {
       fd->buffer_name = strdup(lwb->name);
       fd->buffer_id = lwb->id;
-      fd->buffer_acquisition_time = lwb->acquisition_time;
+      fd->flow = lwb->flow;
    }
    fd->callback_data = callback_data;
    fd->feedback = wp_presentation_feedback(pres->presentation,
diff --git a/src/loader/loader_wayland_helper.h b/src/loader/loader_wayland_helper.h
index 5b5e016..98a9a85 100644
--- a/src/loader/loader_wayland_helper.h
+++ b/src/loader/loader_wayland_helper.h
@@ -27,14 +27,14 @@
 #include <wayland-client.h>
 #include "presentation-time-client-protocol.h"
 #include "util/list.h"
+#include "util/perf/cpu_trace.h"
 
 struct loader_wayland_presentation_feedback_data;
 
 struct loader_wayland_buffer {
    struct wl_buffer *buffer;
    uint32_t id;
-   uint64_t flow_id;
-   uint64_t acquisition_time;
+   struct mesa_trace_flow flow;
    char *name;
 };
 
@@ -86,7 +86,8 @@ void
 loader_wayland_buffer_destroy(struct loader_wayland_buffer *lwb);
 
 void
-loader_wayland_buffer_set_flow(struct loader_wayland_buffer *lwb, uint64_t flow_id);
+loader_wayland_buffer_set_flow(struct loader_wayland_buffer *lwb,
+                               struct mesa_trace_flow *flow);
 
 bool
 loader_wayland_wrap_surface(struct loader_wayland_surface *lws,
diff --git a/src/util/perf/cpu_trace.h b/src/util/perf/cpu_trace.h
index c495dd5..33db845 100644
--- a/src/util/perf/cpu_trace.h
+++ b/src/util/perf/cpu_trace.h
@@ -11,6 +11,12 @@
 
 #include "util/detect_os.h"
 #include "util/macros.h"
+#include "util/os_time.h"
+
+struct mesa_trace_flow {
+   uint64_t id;
+   int64_t start_time;
+};
 
 #if defined(HAVE_PERFETTO)
 
@@ -125,11 +131,15 @@ _mesa_trace_scope_begin(const char *name)
 }
 
 static inline int
-_mesa_trace_scope_flow_begin(const char *name, uint64_t *id)
+_mesa_trace_scope_flow_begin(const char *name,
+			     struct mesa_trace_flow *flow)
 {
-   if (*id == 0)
-      *id = util_perfetto_next_id();
-   _MESA_TRACE_FLOW_BEGIN(name, *id);
+   if (flow->id == 0) {
+      flow->id = util_perfetto_next_id();
+      flow->start_time = os_time_get_nano();
+   }
+
+   _MESA_TRACE_FLOW_BEGIN(name, flow->id);
    _MESA_GPUVIS_TRACE_BEGIN(name);
    return 0;
 }
diff --git a/src/vulkan/wsi/wsi_common_wayland.c b/src/vulkan/wsi/wsi_common_wayland.c
index f497768..484d720 100644
--- a/src/vulkan/wsi/wsi_common_wayland.c
+++ b/src/vulkan/wsi/wsi_common_wayland.c
@@ -1798,7 +1798,7 @@ struct wsi_wl_present_id {
     * which uses frame callback to signal DRI3 COMPLETE. */
    struct wl_callback *frame;
    uint64_t present_id;
-   uint64_t flow_id;
+   struct mesa_trace_flow flow;
    uint64_t submission_time;
    const VkAllocationCallbacks *alloc;
    struct wsi_wl_swapchain *chain;
@@ -2055,9 +2055,9 @@ wsi_wl_swapchain_acquire_next_image_explicit(struct wsi_swapchain *wsi_chain,
                                              uint32_t *image_index)
 {
    struct wsi_wl_swapchain *chain = (struct wsi_wl_swapchain *)wsi_chain;
-   uint64_t id = 0;
+   struct mesa_trace_flow flow = { 0 };
 
-   MESA_TRACE_FUNC_FLOW(&id);
+   MESA_TRACE_FUNC_FLOW(&flow);
 
    /* See comments in queue_present() */
    if (chain->retired)
@@ -2080,7 +2080,7 @@ wsi_wl_swapchain_acquire_next_image_explicit(struct wsi_swapchain *wsi_chain,
    STACK_ARRAY_FINISH(images);
 
    if (result == VK_SUCCESS) {
-      loader_wayland_buffer_set_flow(&chain->images[*image_index].wayland_buffer, id);
+      loader_wayland_buffer_set_flow(&chain->images[*image_index].wayland_buffer, &flow);
       if (chain->suboptimal)
          result = VK_SUBOPTIMAL_KHR;
    }
@@ -2096,9 +2096,9 @@ wsi_wl_swapchain_acquire_next_image_implicit(struct wsi_swapchain *wsi_chain,
    struct wsi_wl_swapchain *chain = (struct wsi_wl_swapchain *)wsi_chain;
    struct timespec start_time, end_time;
    struct timespec rel_timeout;
-   uint64_t id = 0;
+   struct mesa_trace_flow flow = { 0 };
 
-   MESA_TRACE_FUNC_FLOW(&id);
+   MESA_TRACE_FUNC_FLOW(&flow);
 
    /* See comments in queue_present() */
    if (chain->retired)
@@ -2126,7 +2126,7 @@ wsi_wl_swapchain_acquire_next_image_implicit(struct wsi_swapchain *wsi_chain,
             /* We found a non-busy image */
             *image_index = i;
             chain->images[i].busy = true;
-            loader_wayland_buffer_set_flow(&chain->images[i].wayland_buffer, id);
+            loader_wayland_buffer_set_flow(&chain->images[i].wayland_buffer, &flow);
             return (chain->suboptimal ? VK_SUBOPTIMAL_KHR : VK_SUCCESS);
          }
       }
@@ -2321,9 +2321,8 @@ wsi_wl_swapchain_queue_present(struct wsi_swapchain *wsi_chain,
    struct wsi_wl_swapchain *chain = (struct wsi_wl_swapchain *)wsi_chain;
    bool timestamped = false;
    bool queue_dispatched = false;
-   uint64_t flow_id = chain->images[image_index].wayland_buffer.flow_id;
 
-   MESA_TRACE_FUNC_FLOW(&flow_id);
+   MESA_TRACE_FUNC_FLOW(&chain->images[image_index].wayland_buffer.flow);
 
    /* In case we're sending presentation feedback requests, make sure the
     * queue their events are in is dispatched.
